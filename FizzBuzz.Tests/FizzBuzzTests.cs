using FBTDD;
using System;
using Xunit;

namespace FizzBuzz.Tests
{
    public class FizzBuzzTests
    {
        [Fact]
        public void Convert_NonFizzBuzzNumber_ShouldReturnNumberAsString()
        {
            // Arrange
            FizzBuzzConverter fizzBuzz = new FizzBuzzConverter();
            int input = 1;
            string expected = "1";
            // Act
            string actual = fizzBuzz.Convert(input);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Convert_MultipleOfThree_ShouldReturnFizz()
        {
            // Arrange
            FizzBuzzConverter fizzBuzz = new FizzBuzzConverter();
            int input = 3;
            string expected = "Fizz";
            // Act
            string actual = fizzBuzz.Convert(input);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Convert_MultipleOfFive_ShouldReturnBuzz()
        {
            // Arrange
            FizzBuzzConverter fizzBuzz = new FizzBuzzConverter();
            int input = 5;
            string expected = "Buzz";
            // Act
            string actual = fizzBuzz.Convert(input);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Convert_MultipleOfFiveAndThree_ShouldReturnBuzz()
        {
            // Arrange
            FizzBuzzConverter fizzBuzz = new FizzBuzzConverter();
            int input = 15;
            string expected = "FizzBuzz";
            // Act
            string actual = fizzBuzz.Convert(input);
            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
